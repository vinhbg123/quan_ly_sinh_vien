﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmQLDiemSVTheoHocKi : Form
    {
        public frmQLDiemSVTheoHocKi()
        {
            InitializeComponent();
        }

        DataTable tblDiem;

        private void frmQLDiemSVTheoHocKi_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            btnXoa.Enabled = false;
            btnSua.Enabled = false;
            txtHoTen.ReadOnly = true;
            txtLop.ReadOnly = true;
            txtDiemTB.ReadOnly = true;
            txtDiemHe4.ReadOnly = true;
            txtXepLoai.ReadOnly = true;
            string sql2 = "select MaSV, HoTen from tblSINH_VIEN";
            Function.FillCombo(sql2, cboMaSV, "HoTen", "MaSV");
            cboMaSV.SelectedIndex = -1;
            LoadDataGridView();
        }

        private void cboMaSV_TextChanged(object sender, EventArgs e)
        {
            if (cboMaSV.Text == "")
            {
                txtHoTen.Text = "";
                txtLop.Text = "";
                txtDiemTB.Text = "";
            }

            string sql1 = "Select HoTen from tblSINH_VIEN where MaSV = '" + cboMaSV.Text + "'";
            string sql2 = "Select TenLop from tblLOP as a join tblSinh_Vien as b on a.MaLop = b.MaLop where b.MaSV = '" + cboMaSV.Text + "'";
            txtHoTen.Text = Function.GetFileValue(sql1);
            txtLop.Text = Function.GetFileValue(sql2);
            string sql3 = "select avg(a.DiemTongKet) from tblDIEM_HP as a join tblLOP_HOC_PHAN as b on a.MaLopHP = b.MaLopHP where  a.MaSV = '" + cboMaSV.Text + "' and b.HocKi = '" + cboHocKi.Text + "' and b.NamBD = '" + cboNamBD.Text + "' and a.DiemTongKet > 4";
            txtDiemTB.Text = Function.GetFileValue(sql3);
        }

        private void cboHocKi_TextChanged(object sender, EventArgs e)
        {
            if (cboHocKi.Text == "")
                txtDiemTB.Text = "";
            else if (cboNamBD.Text == "")
                txtDiemTB.Text = "";
            else
            {
                string sql3 = "select avg(a.DiemTongKet) from tblDIEM_HP as a join tblLOP_HOC_PHAN as b on a.MaLopHP = b.MaLopHP where  a.MaSV = '" + cboMaSV.Text + "' and b.HocKi = '" + cboHocKi.Text + "' and b.NamBD = '" + cboNamBD.Text + "' and a.DiemTongKet > 4";
                txtDiemTB.Text = Function.GetFileValue(sql3);
            }    
            
        }

        private void cboNamBD_TextChanged(object sender, EventArgs e)
        {
            int nambd;
            if (cboHocKi.Text == "")
                txtDiemTB.Text = "";
            else if (cboNamBD.Text == "")
                txtDiemTB.Text = "";
            else
            {
                string sql3 = "select avg(a.DiemTongKet) from tblDIEM_HP as a join tblLOP_HOC_PHAN as b on a.MaLopHP = b.MaLopHP where  a.MaSV = '" + cboMaSV.Text + "' and b.HocKi = '" + cboHocKi.Text + "' and b.NamBD = '" + cboNamBD.Text + "' and a.DiemTongKet > 4";
                txtDiemTB.Text = Function.GetFileValue(sql3);
            }

            if (cboNamBD.Text == "")
            {
                nambd = 0;
                cboNamBD.Text = "";
            }    
            else
            {
                nambd = Convert.ToInt32(cboNamBD.Text);
                cboNamKT.Text = (nambd + 1).ToString();
            }    
        }

        private void txtDiemTB_TextChanged(object sender, EventArgs e)
        {
            double diemtb;
            string diemhe4, xeploai;
            if (txtDiemTB.Text == "")
            {
                diemtb = 0;
                txtXepLoai.Text = "";
                txtDiemHe4.Text = "";
            }             
            else
                diemtb = Convert.ToDouble(txtDiemTB.Text);

            if (diemtb < 4)
            {
                diemhe4 = "F";
                xeploai = "Kém";
            }    
                

            else if (diemtb >= 4 && diemtb <= 5.4)
            {
                diemhe4 = "D";
                xeploai = "Yếu";
            }    
            else if (diemtb >= 5.5 && diemtb <= 6.9)
            {
                diemhe4 = "C";
                xeploai = "Trung bình";
            }    
                
            else if (diemtb >= 7 && diemtb <= 8.4)
            {
                diemhe4 = "B";
                xeploai = "Khá";
            }    
            else
            {
                diemhe4 = "A";
                xeploai = "Giỏi";
            }
            txtXepLoai.Text = xeploai.ToString();
            txtDiemHe4.Text = diemhe4.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            cboMaSV.Enabled = true;
            cboMaSV.Focus();
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoQua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValue();
        }

        private void ResetValue()
        {
            cboMaSV.Text = "";
            cboMaSV.Text = "";
            txtDiemTB.Text = "";
            cboHocKi.Text = "";
            cboNamBD.Text = "";
            txtDiemRL.Text = "";
            txtDiemHe4.Text = "";
            txtXepLoai.Text = "";
            cboNamKT.Text = "";
        }

        private void LoadDataGridView()
        {
            string sql = "select a.MaSV, b.HoTen, a.HocKi, a.NamBD, a.NamKT,a.DiemRL, a.DiemTB, a.DiemHe4, a.XepLoai from tblDIEM_HK as a, tblSINH_VIEN as b where b.MaSV = a.MaSV";
            tblDiem = Function.GetDataToTable(sql);
            dgvDiemSV.DataSource = tblDiem;
            dgvDiemSV.Columns[0].HeaderText = "Mã sinh viên";
            dgvDiemSV.Columns[1].HeaderText = "Họ tên";
            dgvDiemSV.Columns[2].HeaderText = "Học kì";
            dgvDiemSV.Columns[3].HeaderText = "Năm bắt đầu";
            dgvDiemSV.Columns[4].HeaderText = "Năm kết thúc";
            dgvDiemSV.Columns[5].HeaderText = "Điểm rèn luyện";
            dgvDiemSV.Columns[6].HeaderText = "Điểm trung bình";
            dgvDiemSV.Columns[7].HeaderText = "Điểm hệ 4";
            dgvDiemSV.Columns[8].HeaderText = "Xếp loại";
            dgvDiemSV.AllowUserToAddRows = false;
            dgvDiemSV.EditMode = DataGridViewEditMode.EditProgrammatically;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql; //Lưu lệnh sql
            if (cboMaSV.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải chọn mã sinh viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaSV.Focus();
                return;
            }
            
            if (txtDiemRL.Text == "")
            {
                MessageBox.Show("Bạn phải nhập điểm rèn luyện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiemRL.Focus();
                return;
            }
            if (cboHocKi.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập học kì!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboHocKi.Focus();
                return;
            }
            if (cboNamBD.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập năm bắt đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboHocKi.Focus();
                return;
            }
            sql = "Select MaSV, HocKi, NamBD From tblDIEM_HK where MaSV = '" + cboMaSV.Text + "' and HocKi = N'" + cboHocKi.Text.Trim() + "' and NamBD = '"+cboNamBD.Text+"'";
            if (Function.CheckKey(sql))
            {
                MessageBox.Show("Điểm sinh viên này đã có trong học kì này, bạn phải nhập điểm sinh viên khác!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaSV.Focus();
                return;
            }

            sql = "INSERT INTO tblDiem_HK VALUES(N'" + cboMaSV.Text + "', N'" + cboHocKi.Text + "', N'" + cboNamBD.Text + "', N'" + cboNamKT.Text + "',N'" + txtDiemRL.Text + "',N'" + txtDiemTB.Text + "', N'" + txtDiemHe4.Text + "', N'"+txtXepLoai.Text+"')";
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            ResetValue();
            LoadDataGridView();
            //Nạp lại DataGridView        
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            cboMaSV.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql; //Lưu lệnh sql
            if (cboMaSV.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải chọn mã sinh viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaSV.Focus();
                return;
            }

            if (txtDiemRL.Text == "")
            {
                MessageBox.Show("Bạn phải nhập điểm rèn luyện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiemRL.Focus();
                return;
            }
            if (cboHocKi.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập học kì!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboHocKi.Focus();
                return;
            }
            if (cboNamBD.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập năm bắt đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboHocKi.Focus();
                return;
            }
            sql = "UPDATE tblDIEM_HK SET DiemRL = '"+txtDiemRL.Text+"' where MaSV = '"+cboMaSV.Text+"'and HocKi = '"+cboHocKi.Text+ "'and NamBD = N'"+cboNamBD.Text+"'";
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            MessageBox.Show("Sửa thành công!", "Thông báo");
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            cboMaSV.Enabled = false;
            cboMaSV.Enabled = false;
        }

        private void dgvDiemSV_Click(object sender, EventArgs e)
        {
            cboMaSV.Enabled = false;          
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnBoQua.Enabled = true;
            if (btnThem.Enabled == false)
            {
                MessageBox.Show("Bạn đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            cboMaSV.Text = dgvDiemSV.CurrentRow.Cells["MaSV"].Value.ToString();           
            //txtDiemTB.Text = dgvDiemSV.CurrentRow.Cells["DiemTB"].Value.ToString();
            txtDiemRL.Text = dgvDiemSV.CurrentRow.Cells["DiemRL"].Value.ToString();
            cboHocKi.Text = dgvDiemSV.CurrentRow.Cells["HocKi"].Value.ToString();
            cboNamBD.Text = dgvDiemSV.CurrentRow.Cells["NamBD"].Value.ToString();
            //txtDiemHe4.Text = dgvDiemSV.CurrentRow.Cells["DiemHe4"].Value.ToString();
            //txtXepLoai.Text = dgvDiemSV.CurrentRow.Cells["XepLoai"].Value.ToString();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblDiem.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cboMaSV.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xoá bản ghi này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sql = "DELETE FROM tblDIEM_HK WHERE MaSV = '" + cboMaSV.Text + "' and HocKi = N'" + cboHocKi.Text + "' and NamBD = '"+cboNamBD.Text+"'";
                Function.RunSQL(sql);
                LoadDataGridView();
                ResetValue();
            }
            btnThem.Enabled = true;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            ResetValue();
            cboMaSV.Enabled = true;
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            btnThem.Enabled = true;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            LoadDataGridView();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            btnBoQua.Enabled = true;

            if (cboMaSV.Text == "" && cboHocKi.Text == "" && cboNamBD.Text == "")
            {
                MessageBox.Show("Hãy chọn một điều kiện tìm kiếm!!!", "Yêu cầu ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string sql = "select a.MaSV, b.HoTen, a.HocKi, a.NamBD, a.NamKT,a.DiemRL, a.DiemTB, a.DiemHe4, a.XepLoai from tblDIEM_HK as a, tblSINH_VIEN as b where b.MaSV = a.MaSV ";
            if (cboMaSV.Text != "")
                sql = sql + " AND a.MaSV Like N'%" + cboMaSV.Text + "%'";
            if (cboHocKi.Text != "")
                sql = sql + "AND a.HocKi Like N'%" + cboHocKi.Text + "%'";
            if (cboHocKi.Text != "")
                sql = sql + "AND a.NamBD Like N'%" + cboNamBD.Text + "%'";
            tblDiem = Function.GetDataToTable(sql);
            if (tblDiem.Rows.Count == 0)
            {
                MessageBox.Show("Không có bản ghi thỏa mãn điều kiện!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Có " + tblDiem.Rows.Count + " bản ghi thỏa mãn điều kiện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            dgvDiemSV.DataSource = tblDiem;
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                exportExcel(dgvDiemSV, saveFileDialog1.FileName);
        }
        private void exportExcel(DataGridView dgv, string fileName)
        {
            Microsoft.Office.Interop.Excel.Application excel;
            Microsoft.Office.Interop.Excel.Workbook workbook;
            Microsoft.Office.Interop.Excel.Worksheet worksheet;


            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();

                excel.Visible = true;
                excel.DisplayAlerts = false;
                workbook = excel.Workbooks.Add(Type.Missing);
                excel.Columns.ColumnWidth = 20;
                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets["sheet1"];
                worksheet.Name = "DIEM HOC KI";

                for (int i = 1; i < dgv.Columns.Count + 1; i++)
                {
                    excel.Cells[1, i] = dgv.Columns[i - 1].HeaderText;
                }
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    for (int j = 0; j < dgv.ColumnCount; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = dgv.Rows[i].Cells[j].Value.ToString();
                    }
                }
                workbook.SaveAs(fileName);
                MessageBox.Show("Xuất thành công!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook = null;
                worksheet = null;
            }
        }

        private void cboNamBD_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
