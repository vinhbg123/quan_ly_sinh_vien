﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmQLKhoa : Form
    {
        public frmQLKhoa()
        {
            InitializeComponent();
        }

        DataTable tblKhoa;
        private void frmQLKhoa_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtMaKhoa.Enabled = false;
            LoadDataGridView();
        }

        private void LoadDataGridView()
        {
            string sql = "Select * from tblKHOA";
            tblKhoa = Function.GetDataToTable(sql);
            dgvKhoa.DataSource = tblKhoa;
            dgvKhoa.Columns[0].HeaderText = "Mã khoa";
            dgvKhoa.Columns[1].HeaderText = "Tên khoa";
            dgvKhoa.AllowUserToAddRows = false;
            dgvKhoa.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoQua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValue(); //Xoá trắng các textbox
            txtMaKhoa.Enabled = true; //cho phép nhập mới
            txtMaKhoa.Focus();
        }

        private void ResetValue()
        {
            txtMaKhoa.Text = "";
            txtTenKhoa.Text = "";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql;
            if(txtMaKhoa.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã khoa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaKhoa.Focus();
                return;
            }
            if (txtTenKhoa.Text == "")
            {
                MessageBox.Show("Bạn phải nhập tên khoa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenKhoa.Focus();
                return;
            }
            sql = "Select MaKhoa From tblKHOA where MaKhoa = N'" + txtMaKhoa.Text.Trim() + "'";
            if (Function.CheckKey(sql))
            {
                MessageBox.Show("Mã khoa này đã có, bạn phải nhập mã khác!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMaKhoa.Focus();
                return;
            }

            sql = "INSERT INTO tblKHOA Values(N'" + txtMaKhoa.Text + "' , N'" + txtTenKhoa.Text + "')";
            Function.RunSQL(sql);
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaKhoa.Enabled = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblKhoa.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtMaKhoa.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xoá bản ghi này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sql = "DELETE tblKHOA WHERE MaKhoa = N'" + txtMaKhoa.Text + "'";
                Function.RunSQL(sql);
                LoadDataGridView();
                ResetValue();
            }
        }


        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql;
            if(txtMaKhoa.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã khoa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaKhoa.Focus();
                return;
            }    
            if(txtTenKhoa.Text =="")
            {
                MessageBox.Show("Bạn phải nhập tên khoa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenKhoa.Focus();
                return;
            }
            sql = "UPDATE tblKHOA SET TenKhoa = N'" + txtTenKhoa.Text + "' where MaKhoa = '" + txtMaKhoa.Text + "'";
            Function.RunSQL(sql);
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaKhoa.Enabled = false;
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            ResetValue();         
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtMaKhoa.Enabled = false;
            btnThem.Enabled = true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvKhoa_Click_1(object sender, EventArgs e)
        {
            btnBoQua.Enabled = true;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            txtMaKhoa.Text = dgvKhoa.CurrentRow.Cells["MaKhoa"].Value.ToString();
            txtTenKhoa.Text = dgvKhoa.CurrentRow.Cells["TenKhoa"].Value.ToString();
        }
    }
}
