﻿using Quan_Ly_Sinh_Vien.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmQLSV : Form
    {
        public frmQLSV()
        {
            InitializeComponent();
        }

        DataTable tblSV;
        private void frmQLSV_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtMaSV.Enabled = false;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            string sql1 = "Select MaKhoa, TenKhoa from tblKHOA";
            string sql2 = "Select MaLop, TenLop from tblLOP";
            Function.FillCombo(sql1, cboKhoa, "MaKhoa", "TenKhoa");
            Function.FillCombo(sql2, cboLopHoc, "MaLop", "TenLop");
            string sql3 = "Select MaLop from tblLOP";
            Function.FillCombo(sql3, cboMaLop, "MaLop","TenLop");
            LoadDataGridView();
        }

        private void LoadDataGridView()
        {
            string sql;
            sql = "SELECT * FROM tblSINH_VIEN";
            tblSV = Function.GetDataToTable(sql);
            dgvTTSV.DataSource = tblSV;
            dgvTTSV.Columns[0].HeaderText = "Mã sinh viên";
            dgvTTSV.Columns[1].HeaderText = "Họ tên";
            dgvTTSV.Columns[2].HeaderText = "Ngày sinh";
            dgvTTSV.Columns[3].HeaderText = "Giới tính";
            dgvTTSV.Columns[4].HeaderText = "Địa chỉ";
            dgvTTSV.Columns[5].HeaderText = "Mã lớp";
            dgvTTSV.AllowUserToAddRows = false;
            dgvTTSV.EditMode = DataGridViewEditMode.EditProgrammatically;        
        }
        private void frmQLSV_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            cboMaLop.Enabled = true;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoQua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValue(); //Xoá trắng các textbox
            txtMaSV.Enabled = true; //cho phép nhập mới
            txtMaSV.Focus();
        }

        private  void ResetValue()
        {
            txtMaSV.Text = "";
            txtHoTen.Text = "";
            cboGioiTinh.Text = "";
            txtDiaChi.Text = "";
            cboMaLop.Text = "";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql; //Lưu lệnh sql
            if (txtMaSV.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã sinh viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaSV.Focus();
                return;
            }
            if (txtHoTen.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập họ tên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtHoTen.Focus();
                return;
            }
            if (cboGioiTinh.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập giới tính!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboGioiTinh.Focus();
                return;
            }

            if (txtDiaChi.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập địa chỉ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiaChi.Focus();
                return;
            }
            if (cboMaLop.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã lớp!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaLop.Focus();
                return;
            }
            
            sql = "Select MaSV From tblSINH_VIEN where MaSV = N'" + txtMaSV.Text.Trim() + "'";
            if (Function.CheckKey(sql))
            {
                MessageBox.Show("Mã sinh viên này đã có, bạn phải nhập mã sinh viên khác!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMaSV.Focus();
                return;
            }

            sql = "INSERT INTO tblSINH_VIEN VALUES(N'" + txtMaSV.Text + "', N'" + txtHoTen.Text + "', N'" + dtpNgaySinh.Value.ToString() + "', N'" + cboGioiTinh.Text + "', N'" + txtDiaChi.Text + "',N'" + cboMaLop.Text + "')";
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaSV.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql; //Lưu lệnh sql
            if (txtMaSV.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã sinh viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaSV.Focus();
                return;
            }
            if (txtHoTen.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập họ tên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtHoTen.Focus();
                return;
            }
            if (cboGioiTinh.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập giới tính!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboGioiTinh.Focus();
                return;
            }

            if (txtDiaChi.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập địa chỉ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiaChi.Focus();
                return;
            }
            if (cboMaLop.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã lớp!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaLop.Focus();
                return;
            }
            sql = "UPDATE tblSINH_VIEN SET  HoTen = N'" + txtHoTen.Text + "', NgaySinh = '" + dtpNgaySinh.Value.ToString() + "', GioiTinh = N'" + cboGioiTinh.Text + "', DiaChi = N'" + txtDiaChi.Text + "', MaLop = '" + cboMaLop.Text + "' Where MaSV = '"+ txtMaSV.Text+"'";
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            MessageBox.Show("Sửa thành công!", "Thông báo");
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaSV.Enabled = false;
        }

        private void dgvTTSV_Click(object sender, EventArgs e)
        {
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnBoQua.Enabled = true;
            cboMaLop.Enabled = false;
            txtMaSV.Text = dgvTTSV.CurrentRow.Cells["MaSV"].Value.ToString();
            txtHoTen.Text = dgvTTSV.CurrentRow.Cells["HoTen"].Value.ToString();
            dtpNgaySinh.Text = dgvTTSV.CurrentRow.Cells["NgaySinh"].Value.ToString();
            cboGioiTinh.Text = dgvTTSV.CurrentRow.Cells["GioiTinh"].Value.ToString();
            txtDiaChi.Text = dgvTTSV.CurrentRow.Cells["DiaChi"].Value.ToString();
            cboMaLop.Text = dgvTTSV.CurrentRow.Cells["MaLop"].Value.ToString();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblSV.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtMaSV.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xoá bản ghi này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sql = "DELETE FROM tblSINH_VIEN WHERE MaSV = N'" + txtMaSV.Text + "'";
                Function.RunSQL(sql);
                LoadDataGridView();
                ResetValue();
            }
            btnThem.Enabled = true;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            ResetValue();
            txtMaSV.Enabled = false;
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            btnThem.Enabled = true;
            LoadDataGridView();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            btnBoQua.Enabled = true;

            if (cboKhoa.Text == "" && cboLopHoc.Text == "")
            {
                MessageBox.Show("Hãy nhập một điều kiện tìm kiếm!!!", "Yêu cầu ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string sql = "select a.MaSV, a.HoTen, a.NgaySinh, a.GioiTinh, a.DiaChi, a.MaLop from (tblSinh_Vien as a left join tblLOP as b on a.MaLop = b.MaLop) inner join tblKHOA as c on b.MaKhoa = c.MaKhoa where 1 = 1";
            if (cboKhoa.Text != "")
                sql = sql + " AND c.TenKhoa Like N'%" + cboKhoa.Text + "%'";
            if (cboLopHoc.Text != "")
                sql = sql + "AND b.TenLop Like N'%" + cboLopHoc.Text + "%'";
            tblSV = Function.GetDataToTable(sql);
            if (tblSV.Rows.Count == 0)
            {
                MessageBox.Show("Không có bản ghi thỏa mãn điều kiện!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Có " + tblSV.Rows.Count + " bản ghi thỏa mãn điều kiện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            dgvTTSV.DataSource = tblSV;
        }
    }
}
