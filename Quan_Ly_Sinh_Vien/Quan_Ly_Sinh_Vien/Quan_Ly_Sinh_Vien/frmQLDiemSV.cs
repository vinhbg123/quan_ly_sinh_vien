﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmQLDiemSV : Form
    {
        public frmQLDiemSV()
        {
            InitializeComponent();
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }
        DataTable tblDiem;
        private void frmQLDiemSV_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            btnXoa.Enabled = false;
            btnSua.Enabled = false;
            string sql1 = "select MaLopHP, MaMon from tblLOP_HOC_PHAN";
            Function.FillCombo(sql1, cboMaLopHp,"MaLopHP","MaLopHP");
            cboMaLopHp.SelectedIndex = -1;
            string sql2 = "select MaSV, HoTen from tblSINH_VIEN";
            Function.FillCombo(sql2, cboMaSV, "HoTen", "MaSV");
            cboMaSV.SelectedIndex = -1;
            txtTenMon.ReadOnly = true;
            txtTenLop.ReadOnly = true;
            txtDiemTongKet.ReadOnly = true;
            txtDiemHe4.ReadOnly = true;
            txtHoTen.ReadOnly = true;
            LoadDataGridView();
        }

        private void LoadDataGridView()
        {
            string sql = "select a.MaSV, b.HoTen, a.MaLopHP, a.DiemQT, a.DiemThiLan1, a.DiemTongKet, a.DiemHe4, a.GhiChu from tblDIEM_HP as a, tblSINH_VIEN as b where b.MaSV = a.MaSV order by a.MaSV";
            tblDiem = Function.GetDataToTable(sql);
            dgvDiemSV.DataSource = tblDiem;
            dgvDiemSV.Columns[0].HeaderText = "Mã sinh viên";
            dgvDiemSV.Columns[1].HeaderText = "Họ tên";
            dgvDiemSV.Columns[2].HeaderText = "Mã lớp hp";
            dgvDiemSV.Columns[3].HeaderText = "Điểm quá trình";
            dgvDiemSV.Columns[4].HeaderText = "Điểm thi";
            dgvDiemSV.Columns[5].HeaderText = "Điểm tổng kết";
            dgvDiemSV.Columns[6].HeaderText = "Điểm hệ 4";
            dgvDiemSV.Columns[7].HeaderText = "Ghi chú";
            dgvDiemSV.AllowUserToAddRows = false;
            dgvDiemSV.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void cboMaLopHp_TextChanged(object sender, EventArgs e)
        {
            if (cboMaLopHp.Text == "")
            {
                txtTenLop.Text = "";
                txtTenMon.Text = "";
            }
            string sql1 = "select TenMon from tblMON as a join tblLOP_HOC_PHAN as b on a.MaMon = b.MaMon where b.MaLopHP = '" + cboMaLopHp.Text + "' ";
            txtTenMon.Text = Function.GetFileValue(sql1);
            string sql2 = "select TenLop from tblLOP as a join tblLOP_HOC_PHAN as b on a.MaLop = b.MaLop where b.MaLopHP = '" + cboMaLopHp.Text + "' ";
            txtTenLop.Text = Function.GetFileValue(sql2);
        }

        private void txtDiemTongKet_TextChanged(object sender, EventArgs e)
        {
            double diemtk;
            string diemhe4;
            if (txtDiemTongKet.Text == "")
                diemtk = 0;
            else
                diemtk = Convert.ToDouble(txtDiemTongKet.Text);
            if (diemtk < 4)
                diemhe4 = "F";
            else if (diemtk >= 4 && diemtk <= 5.4)
                diemhe4 = "D";
            else if (diemtk >= 5.5 && diemtk <= 6.9)
                diemhe4 = "C";
            else if (diemtk >= 7 && diemtk <= 8.4)
                diemhe4 = "B";
            else
                diemhe4 = "A";
            txtDiemHe4.Text = diemhe4.ToString();
        }

        private void txtDiemThi_TextChanged(object sender, EventArgs e)
        {
            float diemtk, diemqt, diemthi;
            if (txtDiemQT.Text == "")
                diemqt = 0;
            else
                diemqt = float.Parse(txtDiemQT.Text);
            if (txtDiemThi.Text == "")
                diemthi = 0;
            else
                diemthi = float.Parse(txtDiemThi.Text);
            diemtk =(float)(diemqt * 0.4 + diemthi * 0.6);
            txtDiemTongKet.Text = diemtk.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            cboMaSV.Enabled = true;
            cboMaLopHp.Enabled = true;
            cboMaLopHp.Focus();
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoQua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValue(); //Xoá trắng các textbox    
        }
        private void ResetValue()
        {
            cboMaLopHp.Text = "";
            cboMaSV.Text = "";           
            txtDiemQT.Text = "";
            txtDiemThi.Text = "";
            txtDiemTongKet.Text = "";
            txtDiemHe4.Text = "";
            txtGhiChu.Text = "";
            txtHoTen.Text = "";
        }

       

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql; //Lưu lệnh sql
            if (cboMaLopHp.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải chọn mã lớp học phần!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaLopHp.Focus();
                return;
            }
            if (cboMaSV.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã sinh viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaSV.Focus();
                return;
            }
            if (txtDiemQT.Text == "")
            {
                MessageBox.Show("Bạn phải nhập điểm quá trình!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiemQT.Focus();
                return;
            }
            if (txtDiemThi.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập điểm thi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiemThi.Focus();
                return;
            }

            sql = "Select MaSV, MaLopHP From tblDiem_HP where MaSV = '"+cboMaSV.Text +"' and MaLopHP = N'" + cboMaLopHp.Text.Trim() + "'";
            if (Function.CheckKey(sql))
            {
                MessageBox.Show("Sinh viên này đã có trong lớp học phần, bạn phải nhập mã sinh viên khác!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMaSV.Focus();
                return;
            }

            sql = "INSERT INTO tblDiem_HP VALUES(N'" + cboMaSV.Text + "', N'" + cboMaLopHp.Text + "', N'" + txtDiemQT.Text + "', N'" + txtDiemThi.Text + "', N'" + txtDiemTongKet.Text + "',N'" + txtDiemHe4.Text + "', N'" + txtGhiChu.Text + "')";       
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            ResetValue();
            LoadDataGridView();
            //Nạp lại DataGridView        
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            cboMaSV.Enabled = false;
        }

        private void cboMaSV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboMaSV.Text == "")
                txtHoTen.Text = "";
            string sql = "select hoten from tblSINH_VIEN where MaSV = '" + cboMaSV.Text + "'";
            txtHoTen.Text = Function.GetFileValue(sql);
        }

        private void dgvDiemSV_Click(object sender, EventArgs e)
        {
            cboMaSV.Enabled = false;
            cboMaLopHp.Enabled = false;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnBoQua.Enabled = true;
            if(btnThem.Enabled == false)
            {
                MessageBox.Show("Bạn đang ở chế độ thêm mới!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }    
            cboMaLopHp.Text = dgvDiemSV.CurrentRow.Cells["MaLopHP"].Value.ToString();
            cboMaSV.Text = dgvDiemSV.CurrentRow.Cells["MaSV"].Value.ToString();
            txtDiemQT.Text = dgvDiemSV.CurrentRow.Cells["DiemQT"].Value.ToString();
            txtDiemThi.Text = dgvDiemSV.CurrentRow.Cells["DiemThiLan1"].Value.ToString();
            txtDiemTongKet.Text = dgvDiemSV.CurrentRow.Cells["DiemTongKet"].Value.ToString();
            txtDiemHe4.Text = dgvDiemSV.CurrentRow.Cells["DiemHe4"].Value.ToString();
            txtGhiChu.Text = dgvDiemSV.CurrentRow.Cells["GhiChu"].Value.ToString();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql; //Lưu lệnh sql
            if (cboMaLopHp.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải chọn mã lớp học phần!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaLopHp.Focus();
                return;
            }
            if (cboMaSV.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã sinh viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaSV.Focus();
                return;
            }
            if (txtDiemQT.Text == "")
            {
                MessageBox.Show("Bạn phải nhập điểm quá trình!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiemQT.Focus();
                return;
            }
            if (txtDiemThi.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập điểm thi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiemThi.Focus();
                return;
            }
            sql = "UPDATE tblDIEM_HP SET DiemQT = '" + txtDiemQT.Text + "', DiemThiLan1 = '" + txtDiemThi.Text + "',  DiemTongKet = N'" + txtDiemTongKet.Text + "', DiemHe4 = '" + txtDiemHe4.Text + "', GhiChu = N'" + txtGhiChu.Text + "' Where MaSV = '"+cboMaSV.Text+"'and MaLopHP = '" + cboMaLopHp.Text + "'";
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            MessageBox.Show("Sửa thành công!", "Thông báo");
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            cboMaSV.Enabled = false;
            cboMaLopHp.Enabled = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblDiem.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cboMaLopHp.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xoá bản ghi này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sql = "DELETE FROM tblDIEM_HP WHERE MaSV = '"+cboMaSV.Text+"' and MaLopHP = N'" + cboMaLopHp.Text + "'";
                Function.RunSQL(sql);
                LoadDataGridView();
                ResetValue();
            }
            btnThem.Enabled = true;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            ResetValue();
            cboMaLopHp.Enabled = true;
            cboMaSV.Enabled = true;
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            btnThem.Enabled = true;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            LoadDataGridView();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            btnBoQua.Enabled = true;
            
            if (cboMaLopHp.Text == "" && cboMaSV.Text == "")
            {
                MessageBox.Show("Hãy nhập một điều kiện tìm kiếm!!!", "Yêu cầu ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string sql = "select a.MaSV, b.HoTen, a.MaLopHP, a.DiemQT, a.DiemThiLan1, a.DiemTongKet, a.DiemHe4, a.GhiChu from tblDIEM_HP as a, tblSINH_VIEN as b where a.MaSV = b.MaSV ";
            if (cboMaLopHp.Text != "")
                sql = sql + " AND a.MaLopHP Like N'%" + cboMaLopHp.Text + "%'";
            if (cboMaSV.Text != "")
                sql = sql + "AND a.MaSV Like N'%" + cboMaSV.Text + "%'";
            tblDiem = Function.GetDataToTable(sql);
            if (tblDiem.Rows.Count == 0)
            {
                MessageBox.Show("Không có bản ghi thỏa mãn điều kiện!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Có " + tblDiem.Rows.Count + " bản ghi thỏa mãn điều kiện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            dgvDiemSV.DataSource = tblDiem;
        }

        private void txtDiemQT_TextChanged(object sender, EventArgs e)
        {
            float diemtk, diemqt, diemthi;
            if (txtDiemQT.Text == "")
                diemqt = 0;
            else
                diemqt = float.Parse(txtDiemQT.Text);
            if (txtDiemThi.Text == "")
                diemthi = 0;
            else
                diemthi = float.Parse(txtDiemThi.Text);
            diemtk = (float)(diemqt * 0.4 + diemthi * 0.6);
            txtDiemTongKet.Text = diemtk.ToString();
        }

        private void exportExcel(DataGridView dgv, string fileName)
        {
            Microsoft.Office.Interop.Excel.Application excel;
            Microsoft.Office.Interop.Excel.Workbook workbook;
            Microsoft.Office.Interop.Excel.Worksheet worksheet;
           

            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                
                excel.Visible = true;
                excel.DisplayAlerts = false;
                workbook = excel.Workbooks.Add(Type.Missing);
                excel.Columns.ColumnWidth = 20;
                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets["sheet1"];
                worksheet.Name = "DIEM HOC PHAN";

                for (int i = 1; i < dgv.Columns.Count + 1; i++)
                {
                    excel.Cells[1, i] = dgv.Columns[i - 1].HeaderText;
                }
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    for(int j = 0; j < dgv.ColumnCount; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = dgv.Rows[i].Cells[j].Value.ToString();
                    }    
                }
                workbook.SaveAs(fileName);
                MessageBox.Show("Xuất thành công!");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook = null;
                worksheet = null;
            }
        }
        private void btnIn_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                exportExcel(dgvDiemSV, saveFileDialog1.FileName);
        }
    }
}
