﻿namespace Quan_Ly_Sinh_Vien
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuHeThong = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDangNhap = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDoiMK = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQLNguoiDung = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDangXuat = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDanhMuc = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMonHoc = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuKhoa = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLop = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQuanLy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSinhVien = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuGiangVien = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDiemHP = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDiemHK = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTimKiem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuThongTinSV = new System.Windows.Forms.ToolStripMenuItem();
            this.dToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuThongKe = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDanhSachSV = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDiemTK = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuThoat = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnQLLopHP = new System.Windows.Forms.Button();
            this.btnQLMonHoc = new System.Windows.Forms.Button();
            this.btnQLLop = new System.Windows.Forms.Button();
            this.btnQLKhoa = new System.Windows.Forms.Button();
            this.btnQLDiem = new System.Windows.Forms.Button();
            this.btnGiangVien = new System.Windows.Forms.Button();
            this.btnQLSinhVien = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1483, 100);
            this.panel1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.menuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHeThong,
            this.mnuDanhMuc,
            this.mnuQuanLy,
            this.mnuTimKiem,
            this.mnuThongKe,
            this.mnuThoat});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1481, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuHeThong
            // 
            this.mnuHeThong.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDangNhap,
            this.mnuDoiMK,
            this.mnuQLNguoiDung,
            this.mnuDangXuat,
            this.toolStripMenuItem1,
            this.mnuExit});
            this.mnuHeThong.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuHeThong.Name = "mnuHeThong";
            this.mnuHeThong.Size = new System.Drawing.Size(105, 29);
            this.mnuHeThong.Text = "Hệ thống";
            // 
            // mnuDangNhap
            // 
            this.mnuDangNhap.Name = "mnuDangNhap";
            this.mnuDangNhap.Size = new System.Drawing.Size(268, 30);
            this.mnuDangNhap.Text = "Đăng nhập";
            // 
            // mnuDoiMK
            // 
            this.mnuDoiMK.Name = "mnuDoiMK";
            this.mnuDoiMK.Size = new System.Drawing.Size(268, 30);
            this.mnuDoiMK.Text = "Đổi mật khẩu";
            this.mnuDoiMK.Click += new System.EventHandler(this.mnuDoiMK_Click);
            // 
            // mnuQLNguoiDung
            // 
            this.mnuQLNguoiDung.Name = "mnuQLNguoiDung";
            this.mnuQLNguoiDung.Size = new System.Drawing.Size(268, 30);
            this.mnuQLNguoiDung.Text = "Quản lý người dùng";
            this.mnuQLNguoiDung.Click += new System.EventHandler(this.mnuQLNguoiDung_Click);
            // 
            // mnuDangXuat
            // 
            this.mnuDangXuat.Name = "mnuDangXuat";
            this.mnuDangXuat.Size = new System.Drawing.Size(268, 30);
            this.mnuDangXuat.Text = "Đăng xuất";
            this.mnuDangXuat.Click += new System.EventHandler(this.mnuDangXuat_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(265, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.mnuExit.Size = new System.Drawing.Size(268, 30);
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuDanhMuc
            // 
            this.mnuDanhMuc.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuMonHoc,
            this.mnuKhoa,
            this.mnuLop});
            this.mnuDanhMuc.Name = "mnuDanhMuc";
            this.mnuDanhMuc.Size = new System.Drawing.Size(115, 29);
            this.mnuDanhMuc.Text = "Danh mục";
            // 
            // mnuMonHoc
            // 
            this.mnuMonHoc.Name = "mnuMonHoc";
            this.mnuMonHoc.Size = new System.Drawing.Size(179, 30);
            this.mnuMonHoc.Text = "Môn học ";
            this.mnuMonHoc.Click += new System.EventHandler(this.mnuMonHoc_Click);
            // 
            // mnuKhoa
            // 
            this.mnuKhoa.Name = "mnuKhoa";
            this.mnuKhoa.Size = new System.Drawing.Size(179, 30);
            this.mnuKhoa.Text = "Khoa";
            this.mnuKhoa.Click += new System.EventHandler(this.mnuKhoa_Click);
            // 
            // mnuLop
            // 
            this.mnuLop.Name = "mnuLop";
            this.mnuLop.Size = new System.Drawing.Size(179, 30);
            this.mnuLop.Text = "Lớp";
            this.mnuLop.Click += new System.EventHandler(this.mnuLop_Click);
            // 
            // mnuQuanLy
            // 
            this.mnuQuanLy.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSinhVien,
            this.mnuGiangVien,
            this.mnuDiemHP,
            this.mnuDiemHK});
            this.mnuQuanLy.Name = "mnuQuanLy";
            this.mnuQuanLy.Size = new System.Drawing.Size(94, 29);
            this.mnuQuanLy.Text = "Quản lý";
            // 
            // mnuSinhVien
            // 
            this.mnuSinhVien.Name = "mnuSinhVien";
            this.mnuSinhVien.Size = new System.Drawing.Size(273, 30);
            this.mnuSinhVien.Text = "Sinh viên";
            this.mnuSinhVien.Click += new System.EventHandler(this.mnuSinhVien_Click);
            // 
            // mnuGiangVien
            // 
            this.mnuGiangVien.Name = "mnuGiangVien";
            this.mnuGiangVien.Size = new System.Drawing.Size(273, 30);
            this.mnuGiangVien.Text = "Giảng viên";
            this.mnuGiangVien.Click += new System.EventHandler(this.mnuGiangVien_Click);
            // 
            // mnuDiemHP
            // 
            this.mnuDiemHP.Name = "mnuDiemHP";
            this.mnuDiemHP.Size = new System.Drawing.Size(273, 30);
            this.mnuDiemHP.Text = "Điểm học phần";
            this.mnuDiemHP.Click += new System.EventHandler(this.mnuDiemMH_Click);
            // 
            // mnuDiemHK
            // 
            this.mnuDiemHK.Name = "mnuDiemHK";
            this.mnuDiemHK.Size = new System.Drawing.Size(273, 30);
            this.mnuDiemHK.Text = "Điểm TB theo học kì";
            this.mnuDiemHK.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // mnuTimKiem
            // 
            this.mnuTimKiem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuThongTinSV,
            this.dToolStripMenuItem});
            this.mnuTimKiem.Name = "mnuTimKiem";
            this.mnuTimKiem.Size = new System.Drawing.Size(105, 29);
            this.mnuTimKiem.Text = "Tìm kiếm";
            // 
            // mnuThongTinSV
            // 
            this.mnuThongTinSV.Name = "mnuThongTinSV";
            this.mnuThongTinSV.Size = new System.Drawing.Size(262, 30);
            this.mnuThongTinSV.Text = "Thông tin sinh viên";
            this.mnuThongTinSV.Click += new System.EventHandler(this.mnuThongTinSV_Click);
            // 
            // dToolStripMenuItem
            // 
            this.dToolStripMenuItem.Name = "dToolStripMenuItem";
            this.dToolStripMenuItem.Size = new System.Drawing.Size(262, 30);
            this.dToolStripMenuItem.Text = "Điểm sinh viên";
            this.dToolStripMenuItem.Click += new System.EventHandler(this.dToolStripMenuItem_Click);
            // 
            // mnuThongKe
            // 
            this.mnuThongKe.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDanhSachSV,
            this.mnuDiemTK});
            this.mnuThongKe.Name = "mnuThongKe";
            this.mnuThongKe.Size = new System.Drawing.Size(109, 29);
            this.mnuThongKe.Text = "Thống kê";
            this.mnuThongKe.Click += new System.EventHandler(this.mnuThongKe_Click);
            // 
            // mnuDanhSachSV
            // 
            this.mnuDanhSachSV.Name = "mnuDanhSachSV";
            this.mnuDanhSachSV.Size = new System.Drawing.Size(408, 30);
            this.mnuDanhSachSV.Text = "Danh sách sinh viên được học bổng";
            this.mnuDanhSachSV.Click += new System.EventHandler(this.mnuDanhSachSV_Click);
            // 
            // mnuDiemTK
            // 
            this.mnuDiemTK.Name = "mnuDiemTK";
            this.mnuDiemTK.Size = new System.Drawing.Size(408, 30);
            this.mnuDiemTK.Text = "Danh sách sinh viên phải học lại";
            this.mnuDiemTK.Click += new System.EventHandler(this.mnuDiemTK_Click);
            // 
            // mnuThoat
            // 
            this.mnuThoat.Name = "mnuThoat";
            this.mnuThoat.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.mnuThoat.Size = new System.Drawing.Size(77, 29);
            this.mnuThoat.Text = "Thoát";
            this.mnuThoat.Click += new System.EventHandler(this.mnuThoat_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnQLLopHP);
            this.panel2.Controls.Add(this.btnQLMonHoc);
            this.panel2.Controls.Add(this.btnQLLop);
            this.panel2.Controls.Add(this.btnQLKhoa);
            this.panel2.Controls.Add(this.btnQLDiem);
            this.panel2.Controls.Add(this.btnGiangVien);
            this.panel2.Controls.Add(this.btnQLSinhVien);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 100);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(199, 723);
            this.panel2.TabIndex = 1;
            // 
            // btnQLLopHP
            // 
            this.btnQLLopHP.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnQLLopHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQLLopHP.Location = new System.Drawing.Point(1, 613);
            this.btnQLLopHP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnQLLopHP.Name = "btnQLLopHP";
            this.btnQLLopHP.Size = new System.Drawing.Size(197, 90);
            this.btnQLLopHP.TabIndex = 6;
            this.btnQLLopHP.Text = "QL Lớp HP";
            this.btnQLLopHP.UseVisualStyleBackColor = false;
            this.btnQLLopHP.Click += new System.EventHandler(this.btnQLLopHP_Click);
            // 
            // btnQLMonHoc
            // 
            this.btnQLMonHoc.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnQLMonHoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQLMonHoc.Location = new System.Drawing.Point(-1, 507);
            this.btnQLMonHoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnQLMonHoc.Name = "btnQLMonHoc";
            this.btnQLMonHoc.Size = new System.Drawing.Size(197, 90);
            this.btnQLMonHoc.TabIndex = 5;
            this.btnQLMonHoc.Text = "QL Môn Học";
            this.btnQLMonHoc.UseVisualStyleBackColor = false;
            this.btnQLMonHoc.Click += new System.EventHandler(this.btnQLMonHoc_Click);
            // 
            // btnQLLop
            // 
            this.btnQLLop.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnQLLop.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQLLop.Location = new System.Drawing.Point(0, 406);
            this.btnQLLop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnQLLop.Name = "btnQLLop";
            this.btnQLLop.Size = new System.Drawing.Size(197, 90);
            this.btnQLLop.TabIndex = 4;
            this.btnQLLop.Text = "QL Lớp";
            this.btnQLLop.UseVisualStyleBackColor = false;
            this.btnQLLop.Click += new System.EventHandler(this.btnQLLop_Click);
            // 
            // btnQLKhoa
            // 
            this.btnQLKhoa.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnQLKhoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQLKhoa.Location = new System.Drawing.Point(0, 305);
            this.btnQLKhoa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnQLKhoa.Name = "btnQLKhoa";
            this.btnQLKhoa.Size = new System.Drawing.Size(197, 90);
            this.btnQLKhoa.TabIndex = 3;
            this.btnQLKhoa.Text = "QL Khoa";
            this.btnQLKhoa.UseVisualStyleBackColor = false;
            this.btnQLKhoa.Click += new System.EventHandler(this.btnQLKhoa_Click);
            // 
            // btnQLDiem
            // 
            this.btnQLDiem.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnQLDiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQLDiem.Location = new System.Drawing.Point(0, 203);
            this.btnQLDiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnQLDiem.Name = "btnQLDiem";
            this.btnQLDiem.Size = new System.Drawing.Size(197, 90);
            this.btnQLDiem.TabIndex = 2;
            this.btnQLDiem.Text = "QL Điểm Sinh Viên";
            this.btnQLDiem.UseVisualStyleBackColor = false;
            this.btnQLDiem.Click += new System.EventHandler(this.btnQLDiem_Click);
            // 
            // btnGiangVien
            // 
            this.btnGiangVien.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnGiangVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGiangVien.Location = new System.Drawing.Point(0, 101);
            this.btnGiangVien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnGiangVien.Name = "btnGiangVien";
            this.btnGiangVien.Size = new System.Drawing.Size(197, 90);
            this.btnGiangVien.TabIndex = 1;
            this.btnGiangVien.Text = "QL Giảng Viên";
            this.btnGiangVien.UseVisualStyleBackColor = false;
            this.btnGiangVien.Click += new System.EventHandler(this.btnGiangVien_Click);
            // 
            // btnQLSinhVien
            // 
            this.btnQLSinhVien.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnQLSinhVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQLSinhVien.Location = new System.Drawing.Point(-1, -1);
            this.btnQLSinhVien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnQLSinhVien.Name = "btnQLSinhVien";
            this.btnQLSinhVien.Size = new System.Drawing.Size(197, 90);
            this.btnQLSinhVien.TabIndex = 0;
            this.btnQLSinhVien.Text = "QL Sinh Viên";
            this.btnQLSinhVien.UseVisualStyleBackColor = false;
            this.btnQLSinhVien.Click += new System.EventHandler(this.btnQLSinhVien_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Quan_Ly_Sinh_Vien.Properties.Resources.cover_fanpage_07082020021552019_1m0il5n4_1lb;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(199, 100);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1284, 723);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1483, 823);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý sinh viên uneti";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuHeThong;
        public System.Windows.Forms.ToolStripMenuItem mnuDanhMuc;
        public System.Windows.Forms.ToolStripMenuItem mnuQuanLy;
        private System.Windows.Forms.ToolStripMenuItem mnuTimKiem;
        private System.Windows.Forms.ToolStripMenuItem mnuThongKe;
        private System.Windows.Forms.ToolStripMenuItem mnuThoat;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnQLMonHoc;
        public System.Windows.Forms.Button btnQLLop;
        public System.Windows.Forms.Button btnQLKhoa;
        public System.Windows.Forms.Button btnQLDiem;
        public System.Windows.Forms.Button btnGiangVien;
        public System.Windows.Forms.Button btnQLSinhVien;
        public System.Windows.Forms.ToolStripMenuItem mnuDangNhap;
        public System.Windows.Forms.ToolStripMenuItem mnuDoiMK;
        public System.Windows.Forms.ToolStripMenuItem mnuQLNguoiDung;
        public System.Windows.Forms.ToolStripMenuItem mnuDangXuat;
        public System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem mnuExit;
        public System.Windows.Forms.ToolStripMenuItem mnuMonHoc;
        public System.Windows.Forms.ToolStripMenuItem mnuKhoa;
        public System.Windows.Forms.ToolStripMenuItem mnuLop;
        public System.Windows.Forms.ToolStripMenuItem mnuSinhVien;
        public System.Windows.Forms.ToolStripMenuItem mnuGiangVien;
        public System.Windows.Forms.ToolStripMenuItem mnuDiemHP;
        public System.Windows.Forms.ToolStripMenuItem mnuThongTinSV;
        public System.Windows.Forms.ToolStripMenuItem mnuDanhSachSV;
        public System.Windows.Forms.ToolStripMenuItem mnuDiemTK;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Button btnQLLopHP;
        public System.Windows.Forms.ToolStripMenuItem mnuDiemHK;
        private System.Windows.Forms.ToolStripMenuItem dToolStripMenuItem;
    }
}